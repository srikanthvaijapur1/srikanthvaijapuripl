## IPL Data Project I

### Problem Statement:
##### Download the data from: https://www.kaggle.com/manasgarg/ipl .

In this data assignment you will transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015
   
Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder

### Instructions and Navigations:

All of the code is organized into folders.

#### First, clone the repository:

```bash
git clone  https://gitlab.com/srikanthvaijapur1/srikanthvaijapuripl.git
```
#### Install all the dependencies:

```bash
npm i
```
#### Run the project :
```bash
npm start
```
#### Output:

After running the project you will find your output in 
```src/public/output```