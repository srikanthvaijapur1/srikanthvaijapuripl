const fs = require("fs"); //internal function
const csv = require("csvtojson"); //external function

//@ts-check

const matchesPlayedPerYear = require("./iplFunctions/matchesPlayedPerYear");            //written function
const matchesWonPerTeamPerYear = require("./iplFunctions/matchesWonPerTeamPerYear");    //written function
const extraRunsConcededPerTeam = require("./iplFunctions/extraRunsConcededPerTeam");    //written function
const topEconomicBowlers = require("./iplFunctions/topEconomicBowlers");                //written function 
const tossAndMatchWon = require("./iplFunctions/tossAndMatchWon");                      //written function
const highestPlayerOfMatch = require("./iplFunctions/highestPlayerOfMatch");            //written function
const batsmenStrikeRate = require("./iplFunctions/batsmenStrikeRate");                  //written function

//reading the file

/**
 * This contains path to matches.csv
 * @type {string}  
 */

const matchesFilePath = "src/data/matches.csv";

/**
 * This contains path to deliveries.csv
 * @type {string}  
 */

const deliveriesFilePath = "src/data/deliveries.csv";

//output files(PATH)

const matchesPerYearJson = "src/public/output/matchesPerYear.json";
const matchesWonPerTeamPerYearJson = "src/public/output/matchesWonPerTeamPerYear.Json";
const extraRunsConcededPerTeamJson = "src/public/output/extraRunsConcededPerTeam.Json";
const topEconomicBowlersJson = "src/public/output/topEconomicBowlers.json";
const tossAndMatchWonJson = "src/public/output/tossAndMatchWon.json";
const highestPlayerOfMatchJson = "src/public/output/highestPlayerOfMatch.json";
const batsmenStrikeRateJson = "src/public/output/batsmenStrikeRate.json";


function main() {
    csv()
        .fromFile(matchesFilePath)
        .then((matches) => {
            csv()
                .fromFile(deliveriesFilePath)
                .then((deliveries) => {

                    let matchesPlayedPerYearResult = matchesPlayedPerYear(matches);
                    let matchesWonPerTeamPerYearResult = matchesWonPerTeamPerYear(matches);
                    let extraRunsConcededPerTeamResult = extraRunsConcededPerTeam(matches, deliveries, '2016');
                    let topEconomicBowlersResult = topEconomicBowlers(matches, deliveries, '2015');
                    let tossAndMatchWonResult = tossAndMatchWon(matches);
                    let highestPlayerOfMatchResult = highestPlayerOfMatch(matches);
                    let batsmenStrikeRateResult = batsmenStrikeRate(matches, deliveries, '2015');

                    saveMatchesPerYear(matchesPlayedPerYearResult);
                    saveMatchesWonPerTeamPerYear(matchesWonPerTeamPerYearResult);
                    saveExtraRunsConcededPerTeam(extraRunsConcededPerTeamResult);
                    saveTopEconomyBowlers(topEconomicBowlersResult);
                    saveTossAndMatchWonJson(tossAndMatchWonResult);
                    saveHighestPlayerOfMatch(highestPlayerOfMatchResult);
                    savaBatsmanStrikeRate(batsmenStrikeRateResult);

                });
        });
};

/**
 * @param {matchesPlayedPerYearResult}
 * @param {Object}
 */

function saveMatchesPerYear(matchesPlayedPerYearResult) {

    const jsonData = { matchesPerYear: matchesPlayedPerYearResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(matchesPerYearJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};

/**
 * @param {matchesWonPerTeamPerYearResult}
 * @param {Object}
 */

function saveMatchesWonPerTeamPerYear(matchesWonPerTeamPerYearResult) {

    const jsonData = { matchesWonPerTeamPerYear: matchesWonPerTeamPerYearResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(matchesWonPerTeamPerYearJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};

/**
 * @param {extraRunsConcededPerTeamResult}
 * @param {Object}
 */

function saveExtraRunsConcededPerTeam(extraRunsConcededPerTeamResult) {

    const jsonData = { extraRunsConcededPerTeam: extraRunsConcededPerTeamResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(extraRunsConcededPerTeamJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};

/**
 * @param {topEconomicBowlersResult}
 * @param {Object}
 */

function saveTopEconomyBowlers(topEconomicBowlersResult) {

    const jsonData = { topEconomicBowlers: topEconomicBowlersResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(topEconomicBowlersJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};

/**
 * @param {tossAndMatchWonResult}
 * @param {Object}
 */

function saveTossAndMatchWonJson(tossAndMatchWonResult) {

    const jsonData = { tossAndMatchWon: tossAndMatchWonResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(tossAndMatchWonJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};

/**
 * @param {highestPlayerOfMatchResult}
 * @param {Object}
 */

function saveHighestPlayerOfMatch(highestPlayerOfMatchResult) {

    const jsonData = { highestPlayerOfMatch: highestPlayerOfMatchResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(highestPlayerOfMatchJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};

/**
 * @param {batsmenStrikeRateResult}
 * @param {Object}
 */

function savaBatsmanStrikeRate(batsmenStrikeRateResult) {

    const jsonData = { batsmenStrikeRate: batsmenStrikeRateResult };
    const jsonString = JSON.stringify(jsonData, null, 4);

    fs.writeFile(batsmenStrikeRateJson, jsonString, "utf8", err => {
        if (err) {
            console.log(err);
        };
    });
};




main();
