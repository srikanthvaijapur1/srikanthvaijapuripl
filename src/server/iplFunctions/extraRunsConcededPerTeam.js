/**
 * #### This Module accepts data from matches,deliveries,year and returns Extra Runs Conceded Per Team
 * @module extraRunsConcededPerTeam
 */


/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @param {Object} Deliveries Parsed JSON data of delivers from deliveries.csv file
 * @param {String} Year Parameter
 * @returns {Object} Returns Extra Runs Conceded Per Team
 */

function extraRunsConcededPerTeam(matches, deliveries, year) {

    let teams = {};

    for (let match of matches) {
        if (match.season === year) {

            const id = match.id;

            for (let delivers of deliveries) {
                if (id === delivers.match_id) {

                    let extraRuns = parseInt(delivers.extra_runs);

                    if (teams[delivers.bowling_team]) {
                        teams[delivers.bowling_team] += extraRuns;
                    } else {
                        teams[delivers.bowling_team] = extraRuns;
                    }
                }
            }
        }
    }

    return teams;
}


module.exports = extraRunsConcededPerTeam;