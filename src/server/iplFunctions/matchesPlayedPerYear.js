/**
 * #### This module accepts the data from matches and returns the number of matches played each year
 * @module matchesPlayedPerYear
 * 
 */
/**
 *
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @returns {object} Returns Number of matches played each year 
 */


function matchesPlayedPerYear(matches) {

    let result = {};

    for (let match of matches) {

        const season = match.season;

        if (result[season]) {
            result[season] += 1;
        } else {
            result[season] = 1;
        }
    }

    return result;
}



module.exports = matchesPlayedPerYear;

