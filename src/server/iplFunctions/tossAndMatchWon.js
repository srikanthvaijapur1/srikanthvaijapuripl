/**
 * #### This module accepts the data from matches and returns the team who won highest Number of toss and Match
 * @module tossAndMatchWon
 *
 */
/**
 * 
 * @param {matches} Matches Parsed JSON data of matches from matches.csv file
 * @returns {Object} Return the team who won highest Number of toss and Match
 */

function tossAndMatchWon(matches) {

    let result = {};

    for (let match of matches) {
        if (match.winner === match.toss_winner) {
            if (result[match.winner]) {
                result[match.winner] += 1;
            } else {
                result[match.winner] = 1;
            }
        }
    }

    return result;
}


module.exports = tossAndMatchWon;