/**
 *#### This module accepts the data from matches,deliveries and year as a parameter and returns Top Economic Bowlers of that particular year 
 * @module topEconomicBowlers
 *
 */
/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @param {Object} Deliveries Parsed JSON data of delivers from deliveries.csv file
 * @param {string} Year Parameter
 * @returns {Object} Returns Top Economic Bowlers of that particular year 
 */

function topEconomicBowlers(matches, deliveries, year) {

    let runs = {};
    let balls = {};
    let overs = {};
    let topEconomicBowlers = {};

    for (let match of matches) {
        if (match.season === year) {

            let id = match.id;

            for (let delivers of deliveries) {
                if (id === delivers.match_id) {
                    if (runs[delivers.bowler]) {
                        run = parseInt(delivers.total_runs);
                        runs[delivers.bowler] += run;
                        if (delivers.extra_runs == 0) {
                            if (balls[delivers.bowler]) {
                                balls[delivers.bowler] += 1;
                            } else {
                                balls[delivers.bowler] = 1;
                            }
                        }
                    } else {

                        let run = parseInt(delivers.total_runs);
                        runs[delivers.bowler] = run;

                        if (delivers.extra_runs == 0) {
                            balls[delivers.bowler] = 1;
                        }
                    }
                }
            }
        }
    }

    

    for (let ball in balls) {
        overs[ball] = parseFloat((balls[ball] / 6).toFixed(2));
    }

    let economyBowlers = (Object.values(overs)).sort((a, b) => a - b);
    

    for (let index = 0; index < 10; index++) {
        for (let topBowler in overs) {
            if (overs[topBowler] == economyBowlers[index]) {
                topEconomicBowlers[topBowler] = economyBowlers[index];
            }
        }
    }

    return topEconomicBowlers;
}


module.exports = topEconomicBowlers;