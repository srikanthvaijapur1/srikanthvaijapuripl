/**
 * #### This module accepts the data from matches and returns the highest number of Man of the Match Award for each season
 * @module highestPlayerOfMatch
 *
 */


/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @returns {Object} Returns the highest number of Man of the Match Award for each season
 */

function highestPlayerOfMatch(matches) {

    let result = {};
    let manOfSeries = {};

    for (let match of matches) {

        let years = {};
        let year = match.season;

        for (let player of matches) {
            if (year === player.season) {
                if (years[player.player_of_match]) {
                    years[player.player_of_match] += 1;
                } else {
                    years[player.player_of_match] = 1;
                }
            }
        }

        result[year] = years;
    }

    for (let value in result) {

        let maxVal = (Math.max(...Object.values(result[value])));
        let players = Object.values(result[value]);
        let playerName = Object.keys(result[value]);

        for (let index = 0; index < players.length; index++) {
            if (maxVal === players[index]) {
                manOfSeries[value] = playerName[index];
            }
        }
    }

    return manOfSeries;
}


module.exports = highestPlayerOfMatch;