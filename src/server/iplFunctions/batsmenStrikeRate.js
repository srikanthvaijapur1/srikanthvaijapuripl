/**
 * #### This Module accepts matches,deliveries,year and returns All Batsman Strike rate of that Year
 * @module batsmenStrikeRate
 */

/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @param {Object} Deliveries Parsed JSON data of delivers from deliveries.csv file
 * @param {String} Year Parameter
 * @returns {Object} Returns Batsman Strike rate of that Year
 */

function batsmenStrikeRate(matches, deliveries, year) {

    let runs = {};
    let balls = {};
    let result = {};

    for (let match of matches) {
        if (year == match.season) {

            let id = match.id;

            for (let delivers of deliveries) {
                if (id === delivers.match_id) {
                    if (runs[delivers.batsman]) {
                        runs[delivers.batsman] += parseInt(delivers.batsman_runs);
                        balls[delivers.batsman] += 1;
                    } else {
                        runs[delivers.batsman] = parseInt(delivers.batsman_runs);
                        balls[delivers.batsman] = 1;
                    }
                }
            }
        }
    }

    for (let index in runs) {
        result[index] = parseFloat((runs[index] / balls[index]).toFixed(2));
    }

    return result;
}


module.exports = batsmenStrikeRate;