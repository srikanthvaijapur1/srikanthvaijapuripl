/**
 * #### This module accepts the data from matches and return the number of matches won each team per year
 * @module matchesWonPerTeamPerYear
 *
 */

/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @returns {Object} Returns number of matches won each team per year
 */

function matchesWonPerTeamPerYear(matches) {

    let result = {};

    for (let match of matches) {

        const year = match.season;
        const win = {};

        for (let index = 0; index < matches.length; index++) {
            if (year == matches[index].season) {
                if (win[matches[index].winner]) {
                    win[matches[index].winner] += 1;
                } else {
                    win[matches[index].winner] = 1;
                }
            }
        }

        result[year] = win;

    }

    return result;
}


module.exports = matchesWonPerTeamPerYear